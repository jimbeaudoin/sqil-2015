## Objectif
Roadmap des trucs à faire sur le site de la SQIL 2015.

## TODO

### Mettre sur gitlab
Serait temps...

### Mettre en production (nginx)
Remplacer le site actuel http://2015.sqil.info/

### Grand footer
Avec commanditaires et infos de base.

### Page partenaires
Avec commanditaires, organisateurs d'activités, etc.

### Calendrier des activités
Vue calendrier, liste, json, ical, et géographique.

### Section de nouvelles, style blog
Avec un fil RSS.

* http://harpjs.com/recipes/blog-rss-feed
* http://harpjs.com/recipes/blog-post-excerpts
* http://harpjs.com/recipes/blog-posts-list
* http://harpjs.com/recipes/wrap-markdown-posts

Pour publication vers diaspora*, facebook, twitter, etc.

### Sitemap XML
Pour être gentil avec les engins de recherche.

* http://harpjs.com/recipes/blog-sitemap

### Ajouter un menu au site
Une seule page, pas besoin de menu. Mais on a plus d'ambition que ça :-)

* http://harpjs.com/recipes/templating-dynamic-selected-menu-item

### Ajouter une section pour l'équipe
Une page principale avec une section (ou des sous-pages) pour chaque
personne de l'équipe avec nom, bio, engagements, photo, etc.
