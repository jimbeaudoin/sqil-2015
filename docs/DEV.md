## Dépendances:
* Harpjs (donc, nodejs aussi)
* bower (en nodejs, pour télécharger les dépendances css et js)
* git (ou un autre moyen pour obtenir et contribuer au code)
* Connaissances de base de EJS pour les templates (layout, etc.)
* Connaissances de base de markdown pour le contenu
* Connaissances de base de EJS et bootstrap pour le layout (responsive grid)

## Objectif
Ce document décrit le nécessaire pour contribuer au code (css, structure)
du projet. Un deuxième guide CONTENU.md s'adressera plus particulièrement
à ceux qui veulent contribuer du contenu comme des nouvelles, des activités
ou des pages d'info.

## TODO
*Un TODO pour ce document, DEV.md*

1. Ajouter des liens aux outils utilisés et autres références
1. Ajouter du markdown pour identifier les noms de fichiers, répertoires,
   scripts et outils
1. Ajouter des instructions plus détaillées et markdown pour les commandes
1. Ajouter le contenu de docs/ (incluant ce document) au site web

Consultez TODO.md pour le TODO général (à propos du site).

## Installation
Après un git clone... Vous êtes maintenant prêt à parcourir les sources, le contenu
et le metadata avec votre éditeur préféré.

Avant de lancer le serveur de développement inclus avec harpjs,
allez au répertoire que git vient de créer et lancer "bower install"
pour télécharger les dépendances css et js. Ceci va aussi copier le
répertoire de fonts de bootswatch dans le répertoire public de notre
projet. Voir le fichier .bowerrc pour le script postinstall de bower.

$ harp server

## Développement
Lancer une nouvelle branche git avec git checkout -b MA_BRANCHE
puis éditez le contenu à votre guise. Recharger la page ou naviguez
sur le site et tant que harp server roulera, le site de développement
devrait être accessible à http://localhost:9000 par défaut.

## Compiler (version statique pour la prod.)
Besoin d'une version statique du site? Lancer la commande compile
pour produire le répertoire www avec les fichiers statiques html, css
et JavaScript à partir de vos sources EJS, Markdown et Less.

$ harp compile

## Les branches de développement

### master
La racine, ça représente à peu près ce qui est en ligne puisque
le développement se fait dans les autres branches.

### copie-initiale
C'est la copie (en contenu) de la page http://2015.sqil.info/
mais adaptée à l'environnement harp.

Nous préférons EJS (embedded JavaScript) à Jade pour les templates.
Si harp supportait une version plus moderne de Jade, avec extend
et les blocks, ça serait peut-être différent. Mais Jade ne ressemble
vraiment pas à du HTML, tandis que EJS est plus proche
(JS embedded *dans* du HTML).

Nous préférons Less à Sass bien que ce choix soit plutôt arbitraire.
Nous utilisons bootswatch et sa dépendance bootstrap et tous les deux
sont disponibles autant en Less qu'en Sass ainsi qu'en bon vieux css.

Nous n'utilisons pas encore de JavaScript client (ou serveur, c'est un
site statique, rappelons le!) mais nous préférons préemptivement
ce langage à CoffeeScript.

Les pages de contenu seront préférablement écrites en Markdown tel
qu'implémenté par marked.

### pages
Début de la section des pages d'information supplémentaires, avec
un index généré par un template EJS.

On utilise le sous-répertoire public/pages et un fichier
public/pages/_data.json contenant les titres de certaines pages et
spécifiant le layout à utiliser pour la page index puisque cet index
est généré dans son layout.

Le fichier harp.json est utilisé pour définir un titre par défaut
pour les pages. Ce titre sera remplacé par celui trouvé dans _data.json
selon le contexte.

## CSS / Less
Le look du site repose sur le thème Superhero du projet bootswatch,
qui construit lui-même sur bootstrap. Nous utilisons la version Less
de ces fichiers.

Comme aucun fichier installé par bower n'est référé publiquement, le
répertoire bower_components/ se trouve au même niveau que le répertoire
public/. J'ai menti. Le contenu du répertoire fonts/ de bootstrap est
utilisé, mais nous trichons en le copiant dans public/fonts/ via notre
fichier .bowerrc tel que mentionné ci-haut.

Une copie adaptée de bootstrap.less inclut les imports nécessaires
au thème Superhero de bootswatch et se nomme superhero.less - je sais,
surprenant. On pourra réduire le nombre d'importations dans superhero.less
en retirant les modules bootstrap inutilisés et ainsi réduire la taille
du css généré. Comme superhero.less existe au dessus de public/ il n'est
pas disponible aux visiteurs, mais...

Notre fichier public/styles/main.less par contre import le fameux
superhero.less pour générer /styles/main.css statiquement. Le reste
de main.less défini les règles css spécifiques à notre site.

## Pourquoi bower si ses fichiers ne sont pas publics?
Bonne question! Premièrement, c'est une question d'habitudes, ça vient
d'expériences avec yeoman, grunt et gulp et c'est pratique pour bootswatch.

Maintenant, si bower_components/ se trouve dans public/ il sera compilé
par harp compile. Tout sera compilé! Chaque thème bootswatch, les versions
Less et Sass, etc. Si ça réussi (pas garanti), le tout (une fois compilé)
sera copié dans www/ et ça grossi le résultat pour rien.

À l'inverse, si on met bower_components/ dans public/ mais qu'on le renomme,
public/_bower_components/ le préfixe _ rendant le répertoire (et ses fichiers)
privé, on obtient le résultat actuel, c'est à dire le contenu installé par bower
non-disponible au public - mais importable autrement.

L'option public/_bower_components/ demande un peu plus de changements à
la configuration, il était donc préférable d'utiliser bower_components/
tout simplement.

On pourra revisiter la question quand on utilisera du JavaScript installé
par bower.
